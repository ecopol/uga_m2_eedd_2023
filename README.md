# UGA M2 EEDD 2023 -- course material

## Welcome!

Here you'll find all the material needed for the Experimental Economics course of the 2023 EEDD M2 at UGA.

-   details on how you will be marked ([pdf with all details](Exam_Rules.pdf))
-   Lecture slides & handout ([here](Lectures/))
-   Papers to be used for presentation & reports ([here](Papers/))
-   Instructions, data and results for the class/online experiments ([here](Experiments/))

## Syllabus (sort of)

In this course, we'll mix **lectures** where I'll explain things and **experiments** where we'll play some games together -- usually involving real items and real money -- to get an hands-on understanding of how we do run experiments in economics.

## Exam

No formal exam, but two things to do:

1.  a **6-minute** **presentation** in class on **March 10th**
2.  a **paper report** of \~3 pages to be written by **March 17th**

## Bring your PC

It would be better if you brought your PC to class. Some experiments will be run with paper and pencil, but others will be run online and you'll need to have a PC to do that. Sometimes your phone might be enough, but not always.

## Links for experiments

### Lecture 1

-   Questionnaire 1: <https://forms.gle/GnoTw3SvmX3VRZxM9>

-   Experiment 2: <https://classex.uni-passau.de/bin/>

-   Questionnaire 3: <https://forms.gle/mN8jyXJqFix5zSWa7>

### Lecture 2 part 1: eliciting value

-   WTP coke: <https://forms.gle/v5H5B6zjquZWihDJA>

-   Auction chocolate: <https://forms.gle/Xy9fF1DfYi8v95GH8>

-   Auction oil: <https://forms.gle/mfMZyEiM9tCq5rte6>

-   WTP vs WTA: <https://forms.gle/9v6fynXuGS1R531aA>

-   Social norm: <https://forms.gle/3tvdF6CKQ5w3v4iw7>

### Lecture 2 part 2: social dilemmas

-   PGG: <https://classex.uni-passau.de/> class: INRAE code: M2EEDD

-   Fishing: <https://forms.gle/Fh7DcZHaMvfoskcf9>

-   Extra video at: <https://ncase.me/trust/>

### Lecture 3: consumer biases

-   Attraction effect

    -   instructions: [Experiments/L3_Exp1.pdf](Experiments/L3_Exp1.pdf)

    -   software: <https://gaelexperience.fr/>

-   Retirement plan: <https://classex.uni-passau.de/> class: INRAE code: M2EEDD

-   Trivia: <https://classex.uni-passau.de/> class: INRAE code: M2EEDD

-   Chocolate: <https://classex.uni-passau.de/> class: INRAE code: M2EEDD

#### Lecture 4: markets

-   A competitive market: <http://veconlab.econ.virginia.edu/> (session **pcro7**)

-   Correcting the externality: <http://veconlab.econ.virginia.edu/> (session **pcro8**)
